App.module("Entities", function(Entities, App, Backbone, Marionette, $, _){
  Entities.Index = Backbone.Model.extend({});

  Entities.IndexCollection = Backbone.Collection.extend({
    model: Entities.Index
  });

  var index;

  var initializeIndexs = function(){
    index = new Entities.IndexCollection([
      {
        heading: "Здравствуйте",
        intro: "Это моя первая страница написанная с помощью Marionette, я пока слабо соображаю что тут вобще происходит.",
        url: "#about"
      }
    ]);
  };

  var API = {
    getIndexEntities: function () {
      if (index === undefined) {
        initializeIndexs();
      }
      return index;
    }
  };

  App.reqres.setHandler("index:entities", function(){
    return API.getIndexEntities();
  });

});
