App.module("IndexApp.Intro", function(Intro, App, Backbone, Marionette, $, _){
  Intro.Controller = {
    introIndex: function () {
      var indexs  = App.request('index:entities');

      var indexListView = new Intro.Contacts({
        collection: indexs
      });

      App.regions.main.show(indexListView);
    }
  }
});
