App.module("IndexApp.Intro", function(Intro, App, Backbone, Marionette, $, _){

  Intro.Contact = Marionette.ItemView.extend({
    className: 'hero-unit',
    template: '#hero-info'
  });

  Intro.Contacts = Marionette.CollectionView.extend({
    className: 'container',
    childView: Intro.Contact
  });


});
