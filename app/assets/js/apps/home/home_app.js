App.module("IndexApp", function(IndexApp, App, Backbone, Marionette, $, _){
  IndexApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      "indexs": "introIndex"
    }
  });

  var API = {
    introIndex: function(){
      App.IndexApp.Intro.Controller.introIndex();
      App.execute("set:active:header", "indexs");
    }
  };

  App.on("indexs:intro", function(){
    App.navigate("indexs");
    API.introIndex();
  });

  IndexApp.on("start", function(){
    new IndexApp.Router({
      controller: API
    });
  });
});
