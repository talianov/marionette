App.module("PageApp.Show", function(Show, App, Backbone, Marionette, $, _){
  Show.Message = Marionette.ItemView.extend({
    template: "#page-message"
  });
});
