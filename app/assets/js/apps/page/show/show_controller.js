App.module("PageApp.Show", function(Show, ContactManager, Backbone, Marionette, $, _){
  Show.Controller = {
    showPage: function(){
      var view = new Show.Message();
      App.regions.main.show(view);
    }
  };
});
