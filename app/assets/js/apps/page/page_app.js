App.module("PageApp", function(PageApp, App, Backbone, Marionette, $, _){
  PageApp.Router = Marionette.AppRouter.extend({
    appRoutes: {
      "page" : "showPage"
    }
  });

  var API = {
    showPage: function(){
      PageApp.Show.Controller.showPage();
      App.execute("set:active:header", "page");
    }
  };

  App.on("page:show", function(){
    App.navigate("page");
    API.showPage();
  });

  PageApp.on("start", function(){
    new PageApp.Router({
      controller: API
    });
  });
});
