Что внутри:
----------------
included:
 - Gulp
 - Browserify
 - Babelify
 - Bootstrap
 - jQuery
 - Underscore
 - Backbone
 - Marionette
 - Handlebars
 - BrowserSync
 - Karma
 - Mocha, Chai, Sinon

Установка:
-----
    $ npm install
    $ gulp serve

